import firebase from 'firebase/app';
import 'firebase/storage';

const firebaseConfig = {
    apiKey: "AIzaSyCDWkBADRR81H9cD7ZyFtHFoQ2ycY7XM3E",
    authDomain: "funkostore-8fda1.firebaseapp.com",
    projectId: "funkostore-8fda1",
    storageBucket: "funkostore-8fda1.appspot.com",
    messagingSenderId: "282925646762",
    appId: "1:282925646762:web:20c2a89f37cdb9494de1d3"
};

firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();

export { storage, firebase as default };