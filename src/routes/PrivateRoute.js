import React from "react";
import { Redirect, Route } from "react-router-dom";

export const PrivateRoute = ({ component: Component, ...rest }) => {

	return (
		<Route {...rest} render={props => {
			console.log("Prueba de diseño")
			console.log(localStorage.getItem("user"))
			if (localStorage.getItem("user")) {
				return (<Component {...props} />);
			}
			else {
				return (<Redirect to="/signin" />)
			}
		}} />
	);
}