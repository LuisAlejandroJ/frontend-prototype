import React from "react";
import { Redirect, Route } from "react-router-dom";

export const AdminRoute = ({ component: Component, ...rest }) => {
	var user = JSON.parse(localStorage.getItem("user"));
	return (
		<Route {...rest} render={props => {
			if (user) {
				if(user.rol==="a"){
					return (<Component {...props} />);
				}else{
					alert("You Dont have access")
					return (<Redirect to="/" />)
				}
				
			}
			else {
				return (<Redirect to="/signin" />)
			}
		}} />
	);
}