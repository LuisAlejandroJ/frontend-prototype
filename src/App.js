import './App.css';
import NavBar from './components/Navbar';
import {Switch, BrowserRouter, Route} from 'react-router-dom';
import Products from './components/Products';
import CheckoutPage from './components/CheckoutPage';
import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import PageNotFound from './components/PageNotFound';
import {PrivateRoute} from './routes/PrivateRoute';
import {AdminRoute} from './routes/AdminRoute';
import Checkout from './components/payment/Checkout';
import AddProduct from './components/AddProduct';
import MyOrders from './components/MyOrders';

function App() {

  const checkoutPageView = () => <CheckoutPage />;
  const checkoutPaymentView = () => <Checkout />;
  const myOrdersView = () => <MyOrders />
  const addProductView = () => <AddProduct />;

  return (
    <BrowserRouter>
      <div className="App">
        <NavBar />
        <Switch>
          <Route exact path = "/">
            <Products />
          </Route>
          <Route exact path="/signin">
            <SignIn />
          </Route>
          <Route exact path="/signup">
            <SignUp />
          </Route>
        
          <PrivateRoute exact path="/checkout-page" component={checkoutPageView}/>
          <PrivateRoute exact path="/payment" component={checkoutPaymentView}/>
          <PrivateRoute exact path="/my-orders" component={myOrdersView}/>

          <AdminRoute exact path="/add-product" component={addProductView}/>

          <Route>
            <PageNotFound />
          </Route>

        </Switch>
      </div>   
    
    </BrowserRouter>


  );
}

export default App;
