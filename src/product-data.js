const products = [
    {
      id: 1,
      name: "Pickle Rick",
      productType: "Rick and Morty",
      price: 50000,
      stock: 10,
      image:
        "https://media.takealot.com/covers_tsins/52079139/52079139-1-pdpxl.jpg",
      },
    {
      id: 2,
      name: "Demogorgon",
      productType: "Stranger Things",
      price: 25000,
      stock: 20,
      image:
        "https://m.media-amazon.com/images/I/61+E3bQf-YL._AC_SX569_.jpg",
      },
    {
      id: 3,
      name: "Majin Vegeta",
      productType: "Dragon Ball Z",
      price: 100000,
      stock: 7,
      image:
        "https://http2.mlstatic.com/D_NQ_NP_891755-MCO44528074036_012021-O.jpg",
      },
    {
      id: 4,
      name: "Goku Super Saiyan",
      productType: "Dragon Ball Z",
      price: 80000,
      stock: 7,
      image:
        "https://m.media-amazon.com/images/I/61YcOLaR09L._AC_SL1050_.jpg",
      },    
    {
      id: 5,
      name: "Saitama",
      productType: "One Punch Man",
      price: 30000,
      stock: 8,
      image:
        "https://resources.claroshop.com/medios-plazavip/s2/10790/1307092/5e28f4333675f-5c61093b-d999-42c5-84cd-4f351f7aeacb-1600x1600.jpg",
      },
    {
      id: 6,
      name: "Stan Lee",
      productType: "Iron man",
      price: 12000,
      stock: 3,
      image:
        "https://images-na.ssl-images-amazon.com/images/I/61-CdcTyAWL._AC_SL1000_.jpg",
      },
  ];
export default products;