import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from "@material-ui/icons/Delete";
import accouning from 'accounting';
import { useStateValue } from "../StateProvider";
import { actionTypes } from "../reducer";
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  action: {
    marginTop: "1rem",
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  cardActions: {
    display: "flex",
    justifyContent: "space-between",
    textAlign: "center",
  },
 
}));

export default function CheckoutCard({
    product:{id,quantity,name,productType,image,price,rating,description}
  }) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const [{ basket }, dispatch] = useStateValue();
  var cardItems = JSON.parse(localStorage.getItem('card')) || [];

  function removeOneItem() {
    cardItems.forEach(element => {
      if(element.id===id){
        if(element.quantity===1){
          removeAllItems()
        }else{
          element.quantity-=1;
          localStorage.setItem("card",JSON.stringify(cardItems));
          dispatch({
            type: actionTypes.RELOAD,
          })
        }        
      }
    });
  }

  function addOneItem() {
    cardItems.forEach(element => {
      if(element.id===id){
        element.quantity+=1;
      }
    });
    dispatch({
      type: actionTypes.RELOAD,
    })
    localStorage.setItem("card",JSON.stringify(cardItems));
  }

  function removeAllItems() {
    for(let [i,element] of cardItems.entries()) {
      if(element.id===id){
        cardItems.splice(i,1)
      }
    };
    localStorage.setItem("card",JSON.stringify(cardItems));
    dispatch({
      type: actionTypes.RELOAD,
    })
  }

  return (
    <Card className={classes.root}>
      <CardHeader
        subheader={
          <Typography
            className={classes}
            color='primary'
          >
            {accouning.formatMoney(price,"COP ")}
          </Typography>
        }
        title={name}
      />
      <CardMedia
        className={classes.media}
        image={image}
        title={name}
      />
      <CardActions disableSpacing className={classes.cardActions}>
        <Typography variant="h5">
            Quantity: <strong>{quantity}</strong>
        </Typography>
        <IconButton >
            <RemoveIcon fontSize='large' color="action" onClick={removeOneItem}/>
        </IconButton> 
        <IconButton >
            <AddIcon fontSize='large' color="action" onClick={addOneItem}/>
        </IconButton> 
        <IconButton >
            <DeleteIcon fontSize='large' color="secondary" onClick={removeAllItems}/>
        </IconButton>        
      </CardActions>
    </Card>
  );
}
