import React from "react";
import { withStyles  } from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
//import products from "../product-data"
import API from '../api/api';
import Product from "./Product";

const useStyles = theme => ({
    root: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
});

class Products extends React.Component{
    state = {
        products: []
    }
    
    componentDidMount() {
        API.get(`product.json`)
          .then(res => {
            const products = res.data;
            this.setState({ products });
        })
    }   

    render(){
        const { classes } = this.props;
        return(
            <div className={classes.root}>
                <Grid container spacing={3}>
                    {this.state.products.map((product) => (
                    <Grid item xs={12} sm={6} md={4} lg={3}>
                        <Product key={product.id} product={product}/>
                    </Grid>
                    ))}
                </Grid>
            </div>
        );
    }
};
export default withStyles(useStyles)(Products);