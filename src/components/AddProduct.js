import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import API from '../api/api';
import {storage} from '../firebase';
import Copyright  from './Copyright';


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  margin: {
    margin: theme.spacing(1),
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function AddProduct() {
  const classes = useStyles();

  const [image, setImage] = useState(null);
  const [urlImg, setUrlImg] = useState("");


  function handleUpload(){
    const uploadTask = storage.ref(`images/${image.name}`).put(image);
    uploadTask.on(
      "state_changeg",
      snapshot => {},
      error => {
        console.log(error);
      },
      () => {
        storage
          .ref("images")
          .child(image.name)
          .getDownloadURL()
          .then(url=>{
            setUrlImg(url)
          });
      }
    )
  }

  function changeImagen(e){
    const file = e.target.files[0];
    if(file){
      setImage(file);
    }   
  }

  function handleAddProduct(e){
    e.preventDefault();
    if(!urlImg){
      alert("You need to UPLOAD an image")
    }else{
      const product = {
        id: 7,
        name: document.getElementById("name").value,
        productType: document.getElementById("cathegory").value,
        price: document.getElementById("price").value,
        stock: document.getElementById("stock").value,
        image: urlImg
      }
      console.log(product);
      API.post(`order.json`,{product})
        .then(res => {
          console.log(res);
          console.log(res.data);
        }).catch(function(e){
          console.log(e);
        })
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Add Product !!!
        </Typography>
        <br />
        <div>
          <input type="file" name="imagen" onChange={changeImagen}/>
          <button onClick={handleUpload}>Upload</button>
        </div>
        <br />
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="name"
            label="Product Name"
            name="name"
            autoComplete="name"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="cathegory"
            label="Product Cathegory"
            name="cathegory"
            autoComplete="cathegory"
            autoFocus
          />
          <TextField
            type="number"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="stock"
            label="Product Stock"
            name="stock"
            autoComplete="stock"
            autoFocus
          />
          <TextField
            type="number"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="price"
            label="Product Price (x Unity)"
            name="price"
            autoComplete="price"
            autoFocus
          />
          
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit} 
            onClick={handleAddProduct}       
          >
            ADD
          </Button>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}