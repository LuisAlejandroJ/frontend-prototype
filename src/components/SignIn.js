import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Link as RouteLink, useHistory } from "react-router-dom";
import Copyright  from './Copyright';


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn() {
  const classes = useStyles();

  const validUsers = [
    {
      "userName": "luis",
      "email": "luis@mail.com",
      "password": "123",
      "rol": "c"
    },
    {
      "userName": "alejo",
      "email": "alejo@mail.com",
      "password": "123",
      "rol": "a"
    }
  ]

  function handleSubmit(e) {
    e.preventDefault();
    const emailForm = document.getElementById("email").value
    const passForm = document.getElementById("password").value
    const searchUser = validUsers.find(u=>u.email===emailForm)
    if(searchUser && searchUser.password===passForm){
      const user = {
        name: searchUser.userName,
        rol: searchUser.rol,
        email: document.getElementById("email").value,
      };
      localStorage.setItem("user",JSON.stringify(user))
      window.location.href="/";
    }else{
        alert("check credential!")
    }   
  }
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSubmit}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <RouteLink to="/">
                {"Home"}
              </RouteLink>
            </Grid>
            <Grid item>
                <RouteLink to='/signup'>
                    {"Don't have an account? Sign Up"}
                </RouteLink>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}