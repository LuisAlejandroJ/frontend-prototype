import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { ShoppingCart } from '@material-ui/icons';
import { Badge } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import { red } from '@material-ui/core/colors';
import logo from '../assets/logoCool.png'
import { Link, useHistory } from 'react-router-dom';
import { useStateValue } from "../StateProvider";
import AddBoxIcon from '@material-ui/icons/AddBox';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginBottom: "7rem",
  },
  appBar: {
    backgroundColor: "#F4CA53",
    boxShadow: "none",
  },
  grow: {
    flexGrow: 1,
  },
  button: {
    marginLeft: theme.spacing(2),
  },
  image: {
    marginRight: "10px",
  },
  avatar: {
    marginRight: theme.spacing(2),
    backgroundColor: red[500],
  }
}));

export default function NavBar() {
  const classes = useStyles();
  const history = useHistory();
  const [{ basket }, dispatch] = useStateValue();
  var user = JSON.parse(localStorage.getItem("user"));
  var username = "Guest";
  var icon = "";
  var isAdmin = false;
  var isClient = false;
  if(user){
    var username = user.name;
    if(user.rol==="a"){
      isAdmin = true;
    }else{
      isClient = true;
    }
  }
  if(username.length > 2){
    icon = (username[0]+username[1]).toUpperCase();
  }else{
    icon = ":D";   
  }

  var numberOfElements = 0;
  var cardItems = JSON.parse(localStorage.getItem('card')) || [];
  cardItems.forEach(element => {
    numberOfElements+=element.quantity;
  });
  
  var numberOfElements = 0;
  var cardItems = JSON.parse(localStorage.getItem('card')) || [];
  cardItems.forEach(element => {
    numberOfElements+=element.quantity;
  });
  function OptionIfClient(){
    if(isClient){
      return(
        <Link to="/my-orders">
          <Button variant='outlined'>
            <strong>My Orders</strong>
          </Button>
        </Link>  
      );
    }else{
      return("");
    }
  }

  function OptionIfAdmin(){
    if(isAdmin){
      return(
        <Link to="/add-product">
          <IconButton aria-label="Add product" color="inherit">
            <AddBoxIcon fontSize="large" color="primary" />
          </IconButton> 
        </Link>
      );
    }else{
      return(
        <Link to="/checkout-page">
          <IconButton aria-label="Show cart items" color="inherit">
            <Badge badgeContent={numberOfElements} color="secondary">
              <ShoppingCart fontSize="large" color="primary" />
            </Badge>
          </IconButton> 
        </Link>
      );
    }
  }


  function handleSignOut() {
    if(user){
      localStorage.removeItem('user');    
      //localStorage.clear();
      //console.log(localStorage.getItem("user"));
      window.location.reload();
    }
  }

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Link to="/">
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
              <img 
                src={logo}
                alt='Commerce.js'
                height='50px'
                className={classes.image}
              /> 
            </IconButton>
          </Link>
          <div className={classes.grow} />
          <Avatar aria-label="recipe" className={classes.avatar}>
            {icon}
          </Avatar>
          <Typography variant="h6" color="textPrimary" component="p">
            Hello <strong>{username}</strong> !
          </Typography>
          <div className={classes.button}>
            <Link to={!user && "/signin"}>
              <Button variant='outlined' onClick={handleSignOut}>
                <strong>{user ? "Sign Out" : "Sign In"}</strong>
              </Button>
            </Link>
            <OptionIfClient />
            <OptionIfAdmin />
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
