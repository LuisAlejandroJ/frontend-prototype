import React from "react";
import { Button, makeStyles } from "@material-ui/core";
import accounting from "accounting";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    height: "20vh",
  },
  button: {
    maxWidth: "200px",
    marginTop: "2rem",
  },
}));

export default function Total() {
  const classes = useStyles();
  var numberOfElements = 0;
  var amount = 0;
  var cardItems = JSON.parse(localStorage.getItem('card')) || [];
  cardItems.forEach(element => {
    numberOfElements+=element.quantity;
    amount+=(element.quantity*element.price)
  });
  return (
    <div className={classes.root}>
      <h5>Total items :{numberOfElements}</h5>
      <br />
      <h5>{accounting.formatMoney(amount, "COP ")}</h5>
      <Button
        component={Link}
        to="/payment"
        className={classes.button}
        variant='contained'
        color='secondary'
      >
        Check out
      </Button>
    </div>
  );
};