import React from 'react';
import API from '../api/api';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { withStyles  } from '@material-ui/core/styles';


const useStyles = theme => ({
    table: {
      minWidth: 650,
    },
});

class MyOrders extends React.Component {
  state = {
    orders: []
  }

  componentDidMount() {
    var user = JSON.parse(localStorage.getItem("user"));
    API.get(`/order.json`)
    .then(res => {
        const allOrders = res.data;
        var orders = [];
        for(let i in allOrders){
            if(user.email===allOrders[i].email){
                orders.push(allOrders[i])
            }  
        }
        this.setState({ orders });
    })
  }

  render() {
    return (
        <div>
            <TableContainer component={Paper}>
            <Table size="small" aria-label="a dense table">
                <TableHead>
                <TableRow>
                    <TableCell><strong>Order ID</strong></TableCell>
                    <TableCell align="right">Email</TableCell>
                    <TableCell align="right">Date</TableCell>
                    <TableCell align="right">Amount</TableCell>
                    <TableCell align="right"># Of Items</TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {this.state.orders.map((order) => (
                    <TableRow key={order.id}>
                    <TableCell component="th" scope="row">
                        {order.id}
                    </TableCell>
                    <TableCell align="right">{order.email}</TableCell>
                    <TableCell align="right">{order.date}</TableCell>
                    <TableCell align="right">{order.amount}</TableCell>
                    <TableCell align="right">{order.numitems}</TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
            </TableContainer>
        </div>
    )
  }
}
export default withStyles(useStyles)(MyOrders);